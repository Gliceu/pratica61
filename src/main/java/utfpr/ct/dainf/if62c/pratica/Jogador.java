package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;

public class Jogador {

    private int numero;
    private String nome;

    public Jogador() {
    }

    public Jogador(int numero, String nome) {
        this.numero = numero;
        this.nome = nome;
    }

    @Override
    public String toString() {
        return String.format("%7s-%s", numero, nome);
    }
}
