
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Gliceu <Gliceu@utfpr.edu.br>
 */
public class Pratica61 {

    public static void main(String[] args) {
        Time time1 = new Time();
        Jogador t = new Jogador();
        Time time2 = new Time();
        time1.addJogador("Goleiro", new Jogador(1, "Wilson"));
        time1.addJogador("Atacante", new Jogador(10, "Kleber"));
        time1.addJogador("Meia   ", new Jogador(8, "  Ruan"));
        time2.addJogador("Atacante", new Jogador(9, "Pablo"));
        time2.addJogador("Goleiro", new Jogador(1, "Verweton"));
        time2.addJogador("Meia   ", new Jogador(5, "Otavio"));

        System.out.println("Posição      Time1         Time2");

        Set<String> set = time1.getJogadores().keySet(); 
        Set<String> st = time2.getJogadores().keySet(); 
        for (String key : set) {
            for (String ke : st) {
                if (key == ke) {
                    System.out.printf("%2s%3s%4s\n", key, time1.getJogadores().get(key), time2.getJogadores().get(ke));
                }
            }
        }
    }
}
